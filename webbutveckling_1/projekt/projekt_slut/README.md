# Webbprojekt

## Syfte

I denna uppgift ska du tillämpa de kunskaper du skaffat dig hittills under kursen i ett webbprojekt. Projektet innefattar ett antal delmoment som alla ska vara godkända för att projektet ska bli godkänt. Starta med att läsa igenom hela instruktionen innan du sätter igång.

## Mål

Du ska skapa en webbplats för ett speciellt syfte som du väljer själv. Det kan vara vad som helst, exempelvis:

1. En hemsida som jobbar emot något offentligt API ([se denna lista](apier.md))
2. Ett spel av något slag
3. Hemsida åt en förening, företag eller liknande
4. Internwebb för en företag, företag eller liknande
6. Informationssida om något (gärna något tekniskt)
7. En personlig hemsida exempelvis ett CV eller blogg
8. Något annat kul projekt som skulle lämpa sig som hemsida

## Kravspecifikation

För att ditt webbprojekt ska anses klart måste följande krav uppfyllas:

1. Du ska skriva en utförlig projektrapport som ska lämnas in senast 25 maj 2023.
2. Projektet bör bestå av minst 2 olika sidor.
3. Du måste i projektet skriva egen CSS och HTML, och det måste framgå tydligt i din projektrapport vilken kod som du skrivit och vilken kod du laddat ner. Det är därmed okej att använda sig av CMS och front-end-bibliotek så länge du också skriver egen kod och det tydligt framgår i din projektrapport vad du skrivit själv och inte. Ofta är det lättare att skriva allt själv än att anpassa sig kod någon annan skrivit. 
4. Du får använda dig av både PHP och Javascript om du vill. Men även där bör det tydligt framgå i projektrapporten också där vilken kod som är din egen och vilken kod du lånat från andra.
5. Någonstans ska det finnas ett formulär, t.ex. ett kontaktformulär. (Du behöver inte nödvändigtvis ta hand om informationen som skickas därifrån)
6. Bilder ska finnas med på webbplatsen. Tänk på upphovsrätt, filstorlek och format på de bilder du lägger in.
7. Lägg tonvikt på att göra en snygg och lättläst hemsida. Den ska dessutom fungera och se bra ut i både Firefox, Edge och Google Chrome liksom i såväl dator som mobiltelefon. Det betyder att du ska göra den responsiv.
9. Din kod ska vara snyggt skriven, effektiv och indenterad på korrekt sätt och vara validerad när du lämnar in den. Varningar får förekomma och vara förklarade i projektrapporten. Enstaka fel kan i undantagsfall också tillåtas om det tydligt framgår i projektrapporten vad de beror på och varför du ändå väljer att inte åtgärda dem.

## Projektrapport

Ditt arbete ska dokumenteras i en projektrapport som ska innehålla följande delar:

1. Inledning
    1. Bakgrund
    2. Syfte och mål
    3. Kravspecifikationer
    4. Metod och teknik
    5. Tidsplan
2. Resultat
    1. Konstruktion
    2. Beskrivning
    3. Optimering
    4. Tester
    5. Slutresultat
3. Diskussion
    1. Diskussion
    2. Förbättringsförslag

En projektrapportsmall till Words [hittar du här](webbprojekt.docx).

### Presentation

- Du ska redovisa din sida inför klassen fredag den 12 maj 2023. Sidan behöver inte vara helt klar. du får ytterligare ett par lektioner att slutföra projektet och rapporten.
- Presentationen ska vara på 5-10 minuter och beskriva din produkt och hur den är uppbyggd.
- Tänk på att en dålig presentation kan få en bra produkt att verka dålig och att en bra presentation kan "sälja" en sämre produkt. Därför är det viktigt att lägga ner tid på att förbereda presentationen.
- Var beredd på frågor om hur du löst olika saker på din webbplats.

## Matris

| E | C | A |
| --- | --- | --- |
| Eleven upprättar en enkel projektplan. Eleven utvecklar utifrån planen en produkt i samråd med handledare. I arbetet utvecklar eleven kod som med tillfredsställande resultat följer standarder och omfattar någon av de grundläggande teknikerna för märkspråk och stilmallar. Eleven bearbetar också med viss säkerhet enkel text, bild och eventuell annan media så att de anpassas till produkten. | Eleven upprättar en genomarbetad projektplan. I arbetet utvecklar eleven kod som med tillfredsställande resultat följer standarder och som omfattar några av de grundläggande teknikerna för märkspråk och stilmallar. Eleven bearbetar med viss säkerhet och via flera moment text, bild och eventuell annan media, så att de anpassas till produkten. | Eleven upprättar en genomarbetad projektplan. Eleven utvecklar utifrån planen en produkt efter samråd med handledare. I arbetet utvecklar eleven kod som med gott resultat följer standarder och som omfattar flera av de grundläggande teknikerna för märkspråk och stilmallar. Eleven bearbetar med säkerhet och via flera moment text, bild och eventuell annan media så att de anpassas till produkten. |
| Produkten är av tillfredsställande kvalitet och följer etablerad god praxis vilket eleven kontrollerar med begränsade tester. Eleven vidtar begränsade åtgärder för att åstadkomma snabb överföring av bilder eller andra mediafiler. Dessutom bygger eleven en webbplats som med tillfredsställande resultat följer grundläggande principer för tillgänglighet. | Eleven testar produkten i några webbläsare. Eleven testar också produkten på några plattformar inklusive traditionella datorer eller mobila enheter. Dessutom bygger eleven en webbplats som med tillfredsställande resultat följer grundläggande principer för tillgänglighet. | Produkten är av god kvalitet och följer etablerad god praxis vilket eleven kontrollerar med omfattande tester. Eleven testar produkten i flera webbläsare. Eleven testar också produkten på flera plattformar inklusive traditionella datorer och mobila enheter, vidtar omfattande åtgärder samt optimerar bilder eller andra mediafiler för att åstadkomma snabb överföring av dessa och för att reducera antalet överföringar per sida. Dessutom bygger eleven en webbplats som med gott resultat följer grundläggande principer för tillgänglighet. |
| När arbetet är utfört gör eleven en enkel dokumentation av de moment som har utförts och utvärderar med enkla omdömen sitt arbete och resultat. Eleven använder med viss säkerhet terminologi inom området. | När arbetet är utfört gör eleven en noggrann dokumentation av de moment som har utförts och utvärderar med nyanserade omdömen sitt arbete och resultat. Eleven använder med viss säkerhet terminologi inom området. | När arbetet är utfört gör eleven en noggrann och utförlig dokumentation av de moment som har utförts med koppling till generella principer och testresultat och utvärderar med nyanserade omdömen sitt arbete och resultat samt ger förslag på hur arbetet kan förbättras. Eleven använder med säkerhet terminologi inom området. |