# CMS

I detta kursmoment ska vi installera och redigera en hemsida med CMS-verktyget Wordpress. Ett CMS (Content Management System) är en (enormt) stor mängd redan färdig kod (i detta fall PHP) som ger dig funktioner som att logga in, skapa, redigera och ta bort innehåll samt visa detta innehåll för en besökare.

Några av de mest populära CMS:en som både är gratis och skrivna i PHP är:

- Wordpress - https://sv.wordpress.org/
- Drupal - https://www.drupal.org/
- Joomla - https://www.joomla.org/
- Grav - https://getgrav.org/
- TYPO3 - https://typo3.org/
- Winter CMS - https://wintercms.com/

I regel är allt du behöver en serverplats att lägga ditt CMS på och en databas du kan lagra information i. Sedan bör det bara vara att sätta igång. Respektive hemsida bör vara tillräckligt med hjälp för att komma igång.

## Kravspecifikationer 

- Du måste kunna logga in
- Inloggad användare måste kunna lägga in och redigera inlägg
- Inläggen ska kunna delas upp i kategorier
- Man måste kunna publicera bilder i anslutning till inläggen
- Inläggen ska för en besökare sorteras i kronologisk ordning med det senaste inlägget överst
- Det ska finnas minst tre inlägg på din blogg, med bilder (använd tillåtna bilder från ex https://unsplash.com eller egna bilder)
- Ett inlägg ska förklara nackdelarna och fördelarna med att använda ett CMS istället för att koda sidan helt själv
- Ett inlägg ska förklara vad som skiljer Wordpress från något annat CMS, exempelvis Winter CMS, Drupal eller Joomla t.ex.
- Ett inlägg ska förklara för en besökare hur de kan skapa en egen blogg med hjälp av Wordpress om de har tillgång till en server. Gå igenom:
    1. Hur man laddar ner koden
    2. Hur man laddar upp Wordpress till servern och vad man behöver för att göra det
    3. Hur man installerar Wordpress och vad man behöver för att göra det
    4. Hur man installerar teman och plugins och hur man justerar dem
    5. Hur man skriver ett inlägg och bifogar en bild och vad man bör tänka på när man lägger upp en bild på sin sida (upphovsrätt)

## Instruktioner

1. Besök sidan: https://sv.wordpress.org/ och läs på hur verktyget fungerar och vad det gör
2. Installera Wordpress på din server. Använd installationsinstruktionerna du hittar på: https://wordpress.org/support/article/how-to-install-wordpress/
3. Skapa din blogg och dina inlägg
4. Skapa en användare till mig (mattias.hamberg@ornskoldsvik.se) och ge mig lösenordet rXNrM674hH. Gör att jag kan logga in, men jag behöver inte kunna ändra dina inlägg eller bloggens utseende.
5. Lämna in url:en som inlämningsuppgift till uppgiften projekt_blogg på Vklass