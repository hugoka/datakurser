# CSS - Display

I denna uppgift ska du, i labb-form, lära dig skillnaden på olika display-typer och hur de beter sig på sidan. Innan du börjar kan du läsa på om block- och inlineelement på dessa sidor:

- <https://www.w3schools.com/html/html_blocks.asp>
- <https://www.w3schools.com/css/css_inline-block.asp>

## Instruktioner

Du skall i din `style.css` göra följande och efter varje ändring skall du ladda om sidan och se vad som händer:

1. På taggen span sätter du kantlinjen till 1px solid och färgen till #e7e, vad händer med sidan?
2. På taggen span sätter du padding till 20px, vad händer med sidan?
3. På taggen span sätter du margin till 20px, vad händer med sidan?
4. På taggen span sätter du display till block, vad händer med sidan?
5. Ta bort allt du har gjort på span
6. På taggen div sätter du kantlinjen till 1px solid och färgen till #e00, vad händer med sidan?
7. På taggen div sätter du padding till 20px, vad händer med sidan?
8. På taggen div sätter du margin till 20px, vad händer med sidan?
9. På taggen div sätter du display till inline, vad händer med sidan?
10. Testa lite mer så att du förstår vad inline och block element är och hur padding, border och margin fungerar. Var extra noga med att fatta att marginaler kollapsar till den största av de två som finns på intilliggande taggar.
