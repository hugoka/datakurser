# Gör en kul kluring

I denna uppgift får du helt fria händer att göra en egen "kul kluring"! Mina kula kluringar kan du hitta här nedan. Använd dem gärna som inspiration, men sikta på att göra din egen ännu kulare och ännu klurigare:

1. [Hambergs kluring 1](https://student.oedu.se/~mh6802/kul/mattias_kul_01/)
2. [Hambergs kluring 2](https://student.oedu.se/~mh6802/kul/mattias_kul_02/)
3. [Hambergs kluring 3](https://student.oedu.se/~mh6802/kul/mattias_kul_03/)

Du bör givetvis också titta på genrens mästerverk [Notpron](http://notpron.org/notpron/) som framhålls som internets kanske klurigaste "spel".

## Tips från coachen

- Om du har flera sidor i din mapp, se till att du har en `index.html` som gör att du döljer de andra sidorna
- Använd bilder och CSS för att göra sidan spännande
- Ju nördigare och konstigare sidan är desto kulare blir kluringen i regel
- Ha kul! Det ska vara minst lika kul att bygga kluringen som att lösa den

## När du är klar

När du är klar skickar du givetvis länken till dina vänner via Teams, Discord, Vklass eller liknande. Om du sedan är cool ger du dem inga ledtrådar. De bästa - mest legendariska kluringarna - blir aldrig helt lösta.