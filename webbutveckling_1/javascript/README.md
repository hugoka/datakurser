# Javascript

De kunskaper du ska inhämta till detta kursmoment hittar du på adressen: https://www.w3schools.com/js/default.asp

## Övningar

Övningsuppgifter hittar du på adressen: https://www.w3schools.com/js/js_exercises.asp

## Självtest

Självtestet hittar du på adressen: https://www.w3schools.com/js/js_quiz.asp