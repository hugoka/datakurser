# Bootstrap

De kunskaper du ska inhämta till detta kursmoment hittar du på adressen: https://www.w3schools.com/bootstrap/default.asp

## Övningar

Övningsuppgifter hittar du på adressen: https://www.w3schools.com/bootstrap/bootstrap_exercises.asp

## Självtest

Självtestet hittar du på adressen: https://www.w3schools.com/bootstrap/bootstrap_quiz.asp