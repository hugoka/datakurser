# GIT

De kunskaper du ska inhämta till detta kursmoment hittar du på adressen: https://www.w3schools.com/git/default.asp

## Övningar

Övningsuppgifter hittar du på adressen: https://www.w3schools.com/git/exercise.asp

## Självtest

Självtestet hittar du på adressen: https://www.w3schools.com/git/git_quiz.asp