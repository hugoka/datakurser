# Extra - Laravel

Ett vanligt sätt att påbörja projekt är att man utgår ifrån redan färdig kod - ett så kallat ramverk. Ett vanligt ramverk i PHP-världen är Laravel <https://laravel.com>. Laravel har många fördelar:

- Det är tidsbesparande. På bara några minuter har du en fullt fungerande sida med alla grundläggande funktioner - exempelvis inloggning
- Det är väldokumenterat. Det finns både text och videor som kan visa för dig hur du ska göra för att lösa olika problem och uppgifter
- Det är säkert. Det är nyttigt att koda säkerhetslösningar på egenhand. Men vill man vara säker är det klokt att välja ett beprövat system
- Det är skalbart. Det finns massor med plugins och utbyggnadsmöjligheter
- Det är socialt. När man jobbar med ett ramverk kan man vara i kontakt med och diskutera tillsammans med andra som använder samma verktyg
- Det är lönsamt. Att lära sig och bli duktig på ett system är gångbart på arbetsmarkanden och det blir lättare att få jobb

## Din uppgift

1. Börja gärna med att titta på följande film på Laracast: <https://laracasts.com/series/laravel-8-from-scratch/episodes/3>
2. Installera Laravel i en mapp under `public_html` använd gärna Composer <https://getcomposer.org/> som finns installerat på servern.
3. Följ de instruktioner du kan hitta i dokumentationen <https://laravel.com/docs/8.x/installation>
4. Få det att fungera med din databas genom att skriva in korrekta databas-uppgifter
5. Skapa ett gitrepository för din Laravel-sida. En lämplig `.gitignore` borde redan finnas i mappen efter installatallationen
6. Bekanta dig med Artisan <https://laravel.com/docs/8.x/artisan> som är Laravels Command Line Interface som kan användas till otroligt mycket under byggprocessen
7. Bygg något roligt
