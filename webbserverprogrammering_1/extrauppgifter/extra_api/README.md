# Projekt - API

Nedan följer en lista på spännande och intressanta öppna API:er

| Namn | Länk | Auth |
| --- | --- | --- |
| ColourLovers | http://www.colourlovers.com/api | - |
| ColorMind | http://colormind.io/api-access/ | - |
| Agify | https://agify.io/ | - |
| The Bored API | https://www.boredapi.com/ | - |
| CountAPI | https://countapi.xyz/ | - |
| Host.io | https://host.io/ | Key |
| Gitter | https://developer.gitter.im/ | Key |
| Nationalize | https://nationalize.io/ | - |
| QR Code | https://www.qrtag.net/api/ | - |
| Seralif Color API | https://color.serialif.com/ | - |
| Chuck Norris | https://api.chucknorris.io/ | - |
| Random Useless Facts | https://uselessfacts.jsph.pl/ | - |
| Breezometer | https://docs.breezometer.com/api-documentation/ | Key |
| Fruityvice | https://www.fruityvice.com/ | - |
| Rusty Beer | https://rustybeer.herokuapp.com/docs | - |
| Deck of Cards | https://deckofcardsapi.com/ | - |
| D&D 5e API | https://www.dnd5eapi.co/ | - |
| Open 5e | https://open5e.com/api-docs | - |
| FreeToGame | https://www.freetogame.com/api-doc | - |
| Geek Joke API | https://github.com/sameerkumar18/geek-joke-api | - |
| Joke API | https://v2.jokeapi.dev/ | - |
| jService | http://jservice.io/ | - |
| Airtel IP | https://sys.airtel.lv/ip2country/192.121.39.106/?full=true | - |
| IP-Geo | https://api.techniknews.net/ipgeo/ | - |
| Open Topo Data | https://www.opentopodata.org/ | - |
| REST Countries | https://restcountries.com/ | - |
| What3Words | https://developer.what3words.com/public-api | Key |
| Covid-19 API | https://github.com/M-Media-Group/Covid-19-API | - |
| The Nobel Prize | https://www.nobelprize.org/about/developer-zone-2/ | - |
| Evil Insult Generator | https://evilinsult.com/api/ | - |
| Advice Slip JSON API | https://api.adviceslip.com/ | - |
| Stoicism Quotes | https://github.com/tlcheah2/stoic-quote-lambda-public-api | - |
| Numbers API | http://numbersapi.com/ | - |
| Aztro | https://aztro.readthedocs.io/en/latest/ | - |

För ännu fler API:er kan du kolla in listan på denna sida: <https://github.com/public-apis/public-apis>

## Uppgift

- Gör något kul med något av API:erna ovan
