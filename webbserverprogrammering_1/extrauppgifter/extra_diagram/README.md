# Extra - Diagram

Det kan vara bra att ha någon att diskutera med när man skall designa en databas så jobba gärna ihop i denna uppgift. Det är lätt att missa något när man jobbar ensam.
Du ska göra dina designer i <https://app.diagrams.net/>

Det står att ni efter varje uppgift ska skicka in er databas till mig. Det ska ni inte, ni ska skicka in alla 3 när när är klar alternativt skicka in det ni har hunnit på denna lektion. Jag vill ha in designen av databaserna som PDF filer. `nr1.pdf`, `nr2.pdf`, `nr3.pdf`. Lägg dessa filer i din kursmapp och comitta sedan.

1. Du skall designa en databas för en kursanmälan via web. För att kunna skapa kurser måste man kunna logga in och man skall kunna skapa hur många kurser som helst. Till varje kurs skall man kunna anmäla sig utan att logga in. Fundera ut hur systemet skulle kunna göras och designa sedan en databas. Skicka in din databas till mig. OBS! Jag vill att ni skall lägga till alla fält som ni tycker behövs för att det skall kunna fungera.
2. Du är med och utvecklar ett system och ansvarar för rättigheterna, kunden har krävt att man skall kunna sätta rättigheter på gruppnivå. En person kan givetvis tillhöra flera grupper. Designa denna den del av databasen som har med användare och rättigheter att göra och skicka in den till mig.
3. Din uppgift är att designa en databas som skall användas när följande uppgift skall lösas. Man skall bygga ett enkelt system för lärare där de kan dela med sig av sina lektionsplaneringar. All data skall sparas i databasen. Följande funktionalitet skall finnas:
    - Logga in
    - Lägga till en lektionsplanering med följande informationsfält: planeringsnamn, 
    ämne, nyckelord, kort beskrivning, bifoga valfritt antal filer.
    - Radera planeringar
    - Visa planeringar (via inloggning).
    - Användarregistrering
    - Hantering av glömda lösenord via mail länk
    - Möjlighet till flera administratörer
    - Se andras men bara radera/redigera egna planeringar
    - Kommentarsfunktion, registrerade användare kan lämna kommentarer till en planering
    - ”Betygssättning” av planering (registrerade användare en gång per planering)
    - Möjlighet till icke-publika planeringar (t ex för utkast)
4. Fundera ut hur systemet skulle kunna göras och designa sedan en databas.
5. Skicka in din databas till mig. OBS! Jag vill att ni skall lägga till alla fält som ni tycker behövs för att det skall kunna fungera.