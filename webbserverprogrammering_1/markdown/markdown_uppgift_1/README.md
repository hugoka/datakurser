# Markdown - Uppgift 1

I denna uppgift ska du, med hjälp av Markdown och [The Markdown Guide](https://www.markdownguide.org/basic-syntax/) och [cheatsheet.md](cheatsheet.md) formatera ett dokument så att det efterliknar facit-bilden.

- Länk till [markdown_uppgift_1](markdown_uppgift_1.md)
- Länk till [facit](facit.png)
- Länk till [cheatsheet](cheatsheet.md)

## Instruktioner

1. Läs på hur du kan använda dig av markdown för att utforma ett dokument
2. Ändra i filen `markdown_uppgift_1.md` tills innehållet ser ut som `facit.png`
3. Lägg filen `markdown_uppgift_1.md` i mappen `markdown`på din server och gitta in filen till GitLab
4. Lämna därefter in url:en till din markdownfil på Vklass

**Tips:** Du kan använda dig av VIM för att skriva markdown. Men det blir troligen enklare om du använder dig av online IDE:n på GitLab för att skapa och redigera din markdownfil

**Tips 2:** Vill du vara än mer "Wild and crazy" finns otaliga andra markdownredigerare du kan använda dig av. Testa exempelvis:

- <https://stackedit.io>
- <https://dillinger.io/>
- <https://hackmd.io/>
- <https://onlinemarkdowneditor.dev/>
- <https://markdownmonster.west-wind.com/>
- <https://joplinapp.org/> (mitt favoritprogram)
