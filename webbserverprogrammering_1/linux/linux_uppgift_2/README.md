# Linux - Uppgift 2

## Instruktioner

I denna uppgift ska du få bekanta dig med textredigeraren Vim. Det är en textredigerare du kan använda direkt i terminalen.

1. Gå in på din server via SSH
2. Gå in kursmappen med ex. `cd ~/public_html/webbserverprogrammering_1`
3. I den mappen skriver du `vimtutor sv`.
4. Spara ditt dokument i ditt git-repository med namnet `tutor` med kommandot `:w tutor`
5. Genomför alla övningar fram till och med lektion 4 och rätta till alla exempel. Lektion 5-8 kan du göra i mån av tid och lust.
6. Spara det färdiga dokumentet med kommandot `:w` i Vim
7. Gitta in din kod med följande rader:

~~~~
git status
git add --all
git commit -m "linux_uppgift_2"
git pull
git push
~~~~
