# Linux - Uppgift 4

I denna uppgift ska du lära dig hur du kan använda dig av terminalen för att omdirigera standard out för att skapa nya filer. Utför nedanstående undersökningar en i taget och skriv ner svaren på frågorna i en textfil med namnet `linux_uppgift_4.txt` i mappen `linux` i din webbserverprogrammerings-mapp. Gitta därefter in den på ditt repository.

## Instruktioner

1. Testa att i mappen `linux` på din användare skriva kommandot `echo "Jag lär mig Linux"`. Vad händer?
2. Testa att istället skriva kommandot `echo "Jag lär mig Linux" > trash`. Vad händer nu? Kontrollera innehållet i mappen.
3. Vad är det för skillnad på följande två kommandon?
    1. `echo "Jag lär mig Linux" > trash`
    2. `echo "Jag lär mig Linux" >> trash`
4. På min användare `mh6802` finns det i mappen `public` en fil som heter `folk`. Hur kan du med ett enda kommando skriva för se innehållet i den? Ett tips är att du kan komma åt min användares root-mapp med sökvägen `~mh6802`.
5. Använd ovanstående metod för att omdirigera standard out för att ta innehållet i filen `folk` och skriva in det i filen `trash`. Hur ser ditt kommando ut?
