# Projektnamn

## Bakgrund
- Här beskriver du vad projektet handlar om och vad som ska byggas.
- Hämta gärna lite inspiration från Kursplanen ex. Projekt Glosa är ett projekt som sträcker sig över två kurser, Programmering 2 och Webbserverprogrammering…

## Projektupplägg
- Här kommer det nog att bli flera underrubriker. Beskriv projektets upplägg, alltså vad som måste göras.
- Tala om vilka verktyg som ska som ska användas.
- Skriv ner hur projektet ska kommuniceras med “kunden”.
- Kommunikation med handledaren samt kontinuerlig dokumentation av arbetets gång sker genom…

## Teknisk specifikation
- Lista use cases
- Hur ska GUI se ut? Gör skisser över utseendet.
- Databasdiagram (ev behövs vissa förklaringar/förtydliganden).
- UML Klassdiagram för JAVA-programmet. (Lägger ni in i dokumentet i slutet av projektet)
- Dataflöde och arkitekturdiagram.
- Dokumentation av REST-API:et. Ex på dokumentation av en endpoint:

`/users/login` Skickar login och password för att få en token som kan användas för alla endpoints som kräver detta.
- Skicka
    - `email`
    - `password`
- Svar
    - `token`

## Tidsplan
- Gör en tidsplan utifrån use cases och annat som måste med i planen.
- Tidsplanen skulle kunna göras i ett Gantt-schema (tex i excel).