# SQL

De kunskaper du ska inhämta till detta kursmoment hittar du på adressen: https://www.w3schools.com/mysql/default.asp

## Övningar

Övningsuppgifter hittar du på adressen: https://www.w3schools.com/mysql/mysql_exercises.asp

## Självtest

Självtestet hittar du på adressen: https://www.w3schools.com/mysql/mysql_quiz.asp
